FROM openjdk:8-alpine

# Required for starting application up.
RUN apk update && apk add bash

RUN mkdir -p /opt/app
ENV PROJECT_HOME /opt/app

COPY /target/airport-api-0.0.1-SNAPSHOT.jar $PROJECT_HOME/airport-api.jar

WORKDIR $PROJECT_HOME
CMD ["java", "-jar","./airport-api.jar"]