# Demo Airport API
-----------------------

## Features
--------
This is a Java 8 Rest API to provide some basic airport operations such as:
1. Get the list of all the flights
2. Get a flight by id
3. Get flights by cities and date
4. Create and update a flight

## Design
This application is a Spring Boot Rest Api
The persistence layer is implemented with Spring JPA and the data is in an embedded H2 database
The mapping between entities and Resources are with [modelmapper](http://modelmapper.org/)
The documentation is with Swagger

## Quick Start
1. Make sure your local environment is prepared with Java 1.8+ and maven
2. Clone repository and enter it.

```bash
git clone https://gitlab.com/frank3dev/airport-api.git
cd airport-api
```
3. Build
```bash
mvn clean install
```  

4. Run
```bash  
java -jar target/airport-api-0.0.1-SNAPSHOT.jar
```

5. That's it
```
http://127.0.0.1:9090/airport-api/v1
```

## API documentation
```
Swagger UI: http://localhost:9090/airport-api/swagger-ui.html
Swagger JSON: http://localhost:9090/airport-api/v2/api-docs
```

## Environment set up with Docker
You can use docker to bring up a local environment to work with this application. 
Here you'll find some guides. 

### Pre-requisites

#### Required software
 
 Install the [Docker Community Edition](https://www.docker.com/get-docker) and [Docker-compose](https://docs.docker.com/compose/install/) on your PC, Linux or Windows.

### Compiling the application

```
mvn clean package
```
### Start the docker container

```
docker-compose up
```
### Deployments
By doing so, the application will be automatically deployed upon starting Tomcat.


## Troubleshooting

### BIOS Virtualization
If you're using Docker Toolbox (only way to run docker in Windows versions earlier than 10), then Docker will run on a VirtualBox VM.
Make sure BIOS virtualization is enabled, or you may get errors from VirtualBox.

To enable virtualization:
```
1. reboot computer
2. hit F10 on startup
3. go to Security tab
4. click System Security
5. select "Enable" for the following settings:
  * Virtualization Technology (VTx)
  * Virtualization Technology Directed I/O (VTd)
6. go to main tab
7. click Save and Exit
```

