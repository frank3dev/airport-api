package com.demo.airport;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.demo.airport.controller.ApiEndpoints;
import com.demo.airport.resource.FlightResource;
import com.demo.airport.resource.FlightResource.statusEnum;
import com.google.common.base.Supplier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class FlightControllerTest {

	@LocalServerPort
	private int randomServerPort;

	@Autowired
	private RestTemplate restTemplate;
	
	@Test
	public void getAllFlightsShouldReturnSuccess() throws RestClientException, URISyntaxException {
		ResponseEntity<FlightResource[]> response = restTemplate.getForEntity(getServiceUrl("/flights/"),
				FlightResource[].class);

		Assert.assertTrue("Flight list shouldn't be empty", response.getBody().length > 0);
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void getFlightsByCityAndDateShouldReturnSuccess() throws RestClientException, URISyntaxException {
		ResponseEntity<FlightResource[]> response = restTemplate
				.getForEntity(getServiceUrl("/flights/BOG/MIA/2020-01-30"), FlightResource[].class);

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void getFlightsByCityAndDateShouldReturnNotFound() {

		try {
			restTemplate.getForEntity(getServiceUrl("/flights/BOG/MEL/2020-01-30"), FlightResource[].class);

			Assert.fail();
		} catch (HttpClientErrorException ex) {
			Assert.assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
		} catch (URISyntaxException ue) {
			Assert.fail();
		}
	}

	@Test
	public void getFlightByIdShouldReturnSuccess() throws RestClientException, URISyntaxException {
		ResponseEntity<FlightResource> response = restTemplate.getForEntity(getServiceUrl("/flights/1"),
				FlightResource.class);

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void getFlightByIdShouldReturnNotFound() {
		try {
			restTemplate.getForEntity(getServiceUrl("/flights/50"), FlightResource.class);

			Assert.fail();
		} catch (HttpClientErrorException ex) {
			Assert.assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
		} catch (URISyntaxException ue) {
			Assert.fail();
		}
	}
	
	@Test
	public void updateFlightShouldReturnSuccess() {
		FlightResource flightResource = flightSupplier.get();
	    HttpEntity<FlightResource> entity = new HttpEntity<>(flightResource, new HttpHeaders());
		try {
			ResponseEntity<FlightResource> response = restTemplate.exchange(getServiceUrl("/flights/1"), HttpMethod.PUT, entity, FlightResource.class);
			
			Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
			
		} catch (HttpClientErrorException ex) {
			Assert.fail();
		} catch (URISyntaxException ue) {
			Assert.fail();
		}
	}
	
	@Test
	public void updateFlightShouldReturnBadRequest() {
		//empty resource
		FlightResource flightResource = new FlightResource();
	    HttpEntity<FlightResource> entity = new HttpEntity<>(flightResource, new HttpHeaders());
		try {
			restTemplate.exchange(getServiceUrl("/flights/1"), HttpMethod.PUT, entity, FlightResource.class);
			
			Assert.fail();
		} catch (HttpClientErrorException ex) {
			Assert.assertEquals(HttpStatus.BAD_REQUEST, ex.getStatusCode());
		} catch (URISyntaxException ue) {
			Assert.fail();
		}
	}
	
	@Test
	public void createFlightShouldReturnSuccess() {
	    HttpEntity<FlightResource> entity = new HttpEntity<>(flightSupplier.get(), new HttpHeaders());
		try {
			ResponseEntity<FlightResource> response = restTemplate.postForEntity(getServiceUrl("/flights/"), entity, FlightResource.class);

			System.out.println(response.getBody().get_id());
			Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
			
		} catch (HttpClientErrorException ex) {
			Assert.fail();
		} catch (URISyntaxException ue) {
			Assert.fail();
		}
	}

	@Test
	public void createFlightShouldReturnBadRequest() {
		FlightResource flightResource = new FlightResource();
		flightResource.setStatus("SCHEDULED2");		
	    HttpEntity<FlightResource> entity = new HttpEntity<>(flightResource, new HttpHeaders());
		try {
			restTemplate.exchange(getServiceUrl("/flights/"), HttpMethod.POST, entity, FlightResource.class);
			
			Assert.fail();
		} catch (HttpClientErrorException ex) {
			Assert.assertEquals(HttpStatus.BAD_REQUEST, ex.getStatusCode());
		} catch (URISyntaxException ue) {
			Assert.fail();
		}
	}
	
	private Supplier<FlightResource> flightSupplier = () -> {
		FlightResource resource = new FlightResource();
		resource.setAirline("AmericanAirlines");
		resource.setAirportName("Jose Maria Cordoba");
		resource.setArrivalDate("2020-02-02");
		resource.setArrivalTime("12:00:00");
		resource.setDepartureDate("2020-02-02");
		resource.setDepartureTime("09:00:00");
		resource.setDescription("Test Flight MDE - ZDN");
		resource.setFromCity("MDE");
		resource.setToCity("ZDN");
		resource.setGate("N/A");
		resource.setNumber("AA55");
		resource.setStatus(statusEnum.NEW.toString());
		resource.setStops(0);
		resource.setTicketPrice(0);
		resource.setCurrency("USD");

		return resource;
	};

	private URI getServiceUrl(String servicePath) throws URISyntaxException {
		StringBuilder sb = new StringBuilder("http://localhost:");
		sb.append(randomServerPort);
		sb.append("/airport-api");
		sb.append(ApiEndpoints.V1);
		sb.append(servicePath);
		
		return new URI(sb.toString());
	}

}
