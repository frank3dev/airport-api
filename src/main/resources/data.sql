DROP TABLE IF EXISTS flights;

CREATE TABLE flights (
  id int NOT NULL AUTO_INCREMENT,	
  number varchar(10) NOT NULL,
  airline varchar(20) NOT NULL,
  description varchar(200) NOT NULL,
  from_city varchar(20) NOT NULL,
  to_city varchar(20) NOT NULL,
  departure_date date NOT NULL,
  arrival_date date NOT NULL,
  departure_time time NOT NULL,
  arrival_time time NOT NULL,
  gate varchar(3), 
  stops int NOT NULL,
  airport_name varchar(50),
  ticket_price double,
  currency varchar(3),
  status varchar(20),
  created timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

INSERT INTO flights(number, airline, description, from_city, to_city, departure_date, arrival_date, departure_time, arrival_time, gate, stops, airport_name, ticket_price, currency, status) 
VALUES ('AV147', 'Avianca', 'Flight MDE - BOG Direct', 
'MDE', 'BOG', '2020-01-28', '2020-01-28', '13:00:00', '14:30:00', 'A1', 0, 
'Jose Maria Cordoba', 85, 'USD', 'SCHEDULED');

INSERT INTO flights (number, airline, description, from_city, to_city, departure_date, arrival_date, departure_time, arrival_time, gate, stops, airport_name, ticket_price, currency, status) 
VALUES ('AV148', 'Avianca', 'Flight BOG - MED 1 stop', 
'BOG', 'MED', '2020-01-29', '2020-01-29', '08:00:00', '09:30:00', 'B1', 1, 
'El dorado', 100, 'USD', 'CANCELED');

INSERT INTO flights (number, airline, description, from_city, to_city, departure_date, arrival_date, departure_time, arrival_time, gate, stops, airport_name, ticket_price, currency, status) 
VALUES ('V1026', 'VivaAir', 'Flight BOG - MIA 1 stop', 
'BOG', 'MIA', '2020-01-30', '2020-01-30', '07:00:00', '12:30:00', 'B2', 1, 
'El dorado', 500, 'USD', 'SCHEDULED');

INSERT INTO flights (number, airline, description, from_city, to_city, departure_date, arrival_date, departure_time, arrival_time, gate, stops, airport_name, ticket_price, currency, status) 
VALUES ('A145', 'AmericanAirlines', 'Flight LAX - BOG 1 stop', 
'LAX', 'BOG', '2020-02-10', '2020-02-11', '22:00:00', '03:30:00', 'L1', 1, 
'LAX Airport', 650, 'USD', 'SCHEDULED');

INSERT INTO flights (number, airline, description, from_city, to_city, departure_date, arrival_date, departure_time, arrival_time, gate, stops, airport_name, ticket_price, currency, status) 
VALUES ('AV137', 'Avianca', 'Flight MED - LIM 0 stop', 
'MED', 'LIM', '2020-01-29', '2020-01-29', '13:00:00', '15:30:00', 'G1', 1, 
'Jose Maria Cordoba', 320, 'USD', 'SCHEDULED');

INSERT INTO flights (number, airline, description, from_city, to_city, departure_date, arrival_date, departure_time, arrival_time, gate, stops, airport_name, ticket_price, currency, status) 
VALUES ('AV138', 'Avianca', 'Flight LIM - MED 0 stop', 
'LIM', 'MED', '2020-01-29', '2020-01-29', '13:00:00', '15:30:00', 'G2', 1, 
'Jorge Chavez', 300, 'USD', 'SCHEDULED');