package com.demo.airport.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.entity.Flight;
import com.demo.airport.repo.FlightRepository;
import com.demo.airport.resource.FlightResource;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = ApiEndpoints.V1 + "/flights")
public class FlightController {
	
	@Autowired
	FlightRepository flightRepository;
	
	@Autowired
    private ModelMapper modelMapper;

	public FlightController() { }

	@GetMapping("/")
	@ApiOperation(value = "Get all the flights")
	public ResponseEntity<List<FlightResource>> getFlights() {

		List<FlightResource> flights = StreamSupport.stream(flightRepository.findAll().spliterator(), true)
			.map(flight -> entityToResource(flight))
			.collect(Collectors.toList());

		return new ResponseEntity<>(flights, HttpStatus.OK);
	}

	@GetMapping("/{fromCity}/{toCity}/{date}")
	@ApiOperation(value = "Get flights by cities and date")
	public ResponseEntity<List<FlightResource>> getFlightsByCityAndDate(@PathVariable("fromCity") String fromCity,
			@PathVariable("toCity") String toCity, @PathVariable("date") String date) {
		
		List<Flight> flights = flightRepository.findByFromCityAndToCityAndDepartureDate(fromCity, toCity, modelMapper.map(date, java.sql.Date.class));
		
		if(flights == null || flights.isEmpty()) {
			return notFound().build();
		}
		
		List<FlightResource> result = flights.stream()
			.map(flight -> entityToResource(flight))
			.collect(Collectors.toList());
		
		return ok(result);
	}
	
	@GetMapping("/{flightId}")
	@ApiOperation(value = "Get flight by id")
	public ResponseEntity<FlightResource> getFlightById(@PathVariable("flightId") long flightId) {
		
		Optional<Flight> flight = flightRepository.findById(flightId);
		if(flight.isPresent()) {
			return ok(entityToResource(flight.get()));
		} else {
			return notFound().build();
		}
	}

	@PutMapping("/{flightId}")
	@ApiOperation(value = "Update any field of the flight")
	public ResponseEntity<FlightResource> updateFlight(@PathVariable("flightId") long flightId,
			@Valid @RequestBody FlightResource flightResource) throws URISyntaxException {

		Optional<Flight> flight = flightRepository.findById(flightId);
		if (flight.isPresent()) {
			Flight updateFlight = resourceToEntity(flightResource);
			updateFlight.setId(flightId);
			FlightResource newResource = entityToResource(flightRepository.save(updateFlight));

			return created(new URI(newResource.getId().getHref())).body(newResource);
		} else {
			return notFound().build();
		}
	}
	
	@PostMapping("/")
	@ApiOperation(value = "Create a new flight")
	public ResponseEntity<FlightResource> createFlight(@Valid @RequestBody FlightResource flightResource) throws URISyntaxException {
		
		if(flightResource != null) {
			Flight entity = resourceToEntity(flightResource);
			entity.setStatus(FlightResource.statusEnum.NEW.toString());
			
			Flight createdFlight = flightRepository.save(entity);
			FlightResource newResource = entityToResource(createdFlight);
			
			return created(new URI(newResource.getId().getHref())).body(newResource);
		} else {
			return badRequest().build();
		}
	}
	
	private FlightResource entityToResource(Flight entity) {
		FlightResource resource = new ModelMapper().map(entity, FlightResource.class);
		resource.add(createFlightSelfLink(entity.getId()));

		return resource;
	}	
	
	private Flight resourceToEntity(FlightResource resource) {
		Flight entity = modelMapper.map(resource, Flight.class);
		
		return entity;
	}
	
	private Link createFlightSelfLink(long id) {
		try {
	        return linkTo(methodOn(FlightController.class).getFlightById(id)).withSelfRel();
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
