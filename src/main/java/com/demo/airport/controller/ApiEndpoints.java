package com.demo.airport.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.airport.resource.ApiResource;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiEndpoints.V1)
public class ApiEndpoints {
	public static final String V1 = "/v1";
	
	@GetMapping
	@ApiOperation(value = "Airport API Entry Point")
	public ResponseEntity<ApiResource> entryPoint(){
		return ResponseEntity.ok(new ApiResource());
	}
}
