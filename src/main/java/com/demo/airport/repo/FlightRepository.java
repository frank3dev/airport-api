package com.demo.airport.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.demo.airport.entity.Flight;

public interface FlightRepository extends CrudRepository<Flight, Long> {

	List<Flight> findByFromCityAndToCityAndDepartureDate(String fromCity, String toCity, Date departureDate);

}
