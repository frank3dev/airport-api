package com.demo.airport;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.demo.airport.entity.Flight;
import com.demo.airport.resource.FlightResource;

@SpringBootApplication
public class AirportApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirportApiApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.addMappings(new PropertyMap<FlightResource, Flight>() {
			@Override
			protected void configure() {
				map().setId(source.get_id() != null ? Long.parseLong(source.get_id()) : null);
			}
		});
		modelMapper.addMappings(new PropertyMap<Flight, FlightResource>() {
			@Override
			protected void configure() {
				map().set_id(source.getId() != null ? source.getId().toString(): null);
			}
		});

		return new ModelMapper();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
