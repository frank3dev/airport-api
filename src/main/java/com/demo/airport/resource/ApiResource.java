package com.demo.airport.resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.demo.airport.controller.FlightController;

public class ApiResource extends ResourceSupport {
	
	public static final String FLIGHTS_REL = "flights";

	public ApiResource() {
		try {
            this.add(flightLink());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
	}
	
	private Link flightLink() {
		try {
	        return linkTo(methodOn(FlightController.class).getFlights()).withRel(FLIGHTS_REL);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

}
